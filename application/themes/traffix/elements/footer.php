<?php defined('C5_EXECUTE') or die("Access Denied.");

?>

    <footer id="ft">
        <?php
        $a = new GlobalArea('Footer Contact');
        $a->display();
        ?>
        <div class="footer-bottom container-fluid">
            <div class="row">
                <div class="col-sm-4">
                    <a class="navbar-brand" href="/"><img src="<?=$view->getThemePath(); ?>/images/logo.png" alt="Braggs Signs" /></a>
                </div>
                <div class="col-sm-4">
                <?php
                $a = new GlobalArea('Footer Middle');
                $a->display();
                ?>
                </div>
                <div class="col-sm-4">
                    <a id="back-to-top" href="#top">Back to top</a>
                </div>
            </div>
        </div>
    </footer>
</div>
<?php Loader::element('footer_required'); ?>
</body>
</html>

