(function($) {
	$(document).ready(function() {
		function updatePositions() {
			
		}
		updatePositions();
		var resize_timer;
		$(window).resize(function() {
			clearTimeout(resize_timer);
			resize_timer = setTimeout(updatePositions, 100);
		});
	});
})(jQuery);

function getHashFilter() {
  var matches = location.hash.match(/filter=([^&]+)/i); // get filter=filterName
  var hashFilter = matches && matches[1];
  return hashFilter && decodeURIComponent(hashFilter);
}
function intval(v) {
    v = parseInt(v);
    return isNaN(v) ? 0 : v;
}
function floatval(v) {
    v = parseFloat(v);
    return isNaN(v) ? 0 : v;
}
function getDigits(v) {
    return v.replace(/[^\d]/g, '');
}
function slug(string) {
    if(typeof string == 'undefined') string = '';
    return string.replace(/(^\-+|[^a-zA-Z0-9\/_| -]+|\-+$)/g, '').toLowerCase().replace(/[\/_| -]+/g, '-');
}
function randomint(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
Number.prototype.map = function(in_min, in_max, out_min, out_max) {
    return (this - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
Number.prototype.formatMoney = function(c, d, t) { /* (decimal places, decimal, thousands separator */
    var n = this, c = isNaN(c = Math.abs(c)) ? 2 : c, d = d == undefined ? "." : d, t = t == undefined ? "," : t, s = n < 0 ? "-" : "", i = parseInt(n = Math.abs(+n || 0).toFixed(c)) + "", j = (j = i.length) > 3 ? j % 3 : 0;
    return s + (j ? i.substr(0, j) + t : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + t) + (c ? d + Math.abs(n - i).toFixed(c).slice(2) : "");
};
function getMoney(strMoney) {
    return floatval(floatval(jQuery.trim(strMoney.replace(/[^\d\.]/g, ''))).toFixed(2));
}
function shuffle(v) {
    for(var j, x, i = v.length; i; j = parseInt(Math.random() * i), x = v[--i], v[i] = v[j], v[j] = x);
    return v;
}
function capitalize(str) {
    return str.replace(/\w\S*/g, function(txt) {return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
}
jQuery.easing.easeOutExpo = function (x, t, b, c, d) {
	return (t==d) ? b+c : c * (-Math.pow(2, -10 * t/d) + 1) + b;
}

